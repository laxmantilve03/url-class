## The task

This repository includes a single class file, responsible for managing a url string and it's component parts.

There are 5 steps to this test:

1. Using a regex to parse a URL is very flawed, can you refactor the class to parse a URL without using regex?
2. Can you create functionality that can switch the URL scheme from HTTP to HTTPS and vice versa.
3. Can you create functionality that can add and remove query parameters.
4. Can you create functionality to return the URL with any changes made through the previously added functionality.
5. Can you create functionality to compare 2 URL objects. It should perform 2 different comparisons, firstly it should be able to compare 2 URLs are exactly the same, and second it should be able to compare if 2 URLs are the same while ignoring the query string.

You do not need to spend more than a couple of hours on this. Keep good design principles in mind.


<!-- Author : Laxman -->

 Q1 : parse a URL without using regex
 
 used parse_url() function to parse the url.

 Q2 : switch the URL scheme from HTTP to HTTPS and vice versa.
    use the switchScheme method to switch between HTTP and HTTPS schemes.

    Example :

    echo $url->getScheme();  // Output: http

    $url->switchScheme();
    
    echo $url->getScheme();  // Output: https


Q3 : functionality that can add and remove query parameters.
    addQueryParam for adding query param and removeQueryParam for removing query param

    Example :   

    $url->addQueryParam('param1', 'value1');
    $url->addQueryParam('param2', 'value2');

    echo $url->getUrl();
    // Output: http://test.com/path?param1=value1&param2=value2

    $url->removeQueryParam('param1');

    echo $url->getUrl();
    // Output: http://test.com/path?param2=value2


Q4 : functionality to return the URL with any changes made through the previously added         functionality.
    The getUrl method already returns the URL with any changes made through the previously added functionality. You can use it to retrieve the updated URL after modifying the scheme or query parameters.

    Example :

    echo $url->getUrl();
    Output: http://test.com/path?param1=value1&param2=value2

    $url->removeQueryParam('param1');

    echo $url->getUrl();
    Output: http://test.com/path?param2=value2

    $url->switchScheme();

    echo $url->getUrl();
    Output: https://test.com/path?param2=value2 //Updated Url

Q5 : functionality to compare 2 URL objects. It should perform 2 different comparisons, firstly it should be able to compare 2 URLs are exactly the same, and second it should be able to compare if 2 URLs are the same while ignoring the query string.

    By default, the compareUrl method will ignore query parameters ($ignoreQueryParams is set to true). If you want to compare URLs considering query parameters, you can pass false as the second argument when calling the method.
   