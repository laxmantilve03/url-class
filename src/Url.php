<?php

final class Url
{
    private string $scheme;
    private string $host;
    private string $path;
    private array $query;

    public function __construct(string $url)
    {
        // Author : Laxman
        // used parse_url() function to parse the url.
        $parsedUrl = parse_url($url);

        $this->scheme = $parsedUrl['scheme'] ?? '';
        $this->host = $parsedUrl['host'] ?? '';
        $this->path = $parsedUrl['path'] ?? '';
        $this->query = $this->parseQuery($parsedUrl['query'] ?? '');
    }

    public function getScheme(): string
    {
        return $this->scheme;
    }

    public function getHost(): string
    {
        return $this->host;
    }

    public function getPath(): string
    {
        return $this->path;
    }

    public function getQuery(): array
    {
        return $this->query;
    }

    
    public function getUrl(): string
    {
        $query = $this->buildQuery($this->query);
        $queryString = $query !== '' ? "?{$query}" : '';

        return "{$this->scheme}://{$this->host}{$this->path}{$queryString}";
    }

    // Author : Laxman
    // use the switchScheme method to switch between HTTP and HTTPS schemes.

    public function switchScheme(): void
    {
        if ($this->scheme === 'http') {
            $this->scheme = 'https';
        } elseif ($this->scheme === 'https') {
            $this->scheme = 'http';
        }
    }

    // Author : Laxman
    // addQueryParam for adding query param and removeQueryParam for removing query param
    public function addQueryParam(string $key, string $value): void
    {
        $this->query[$key] = $value;
    }

    // Author : Laxman
    // removeQueryParam for removing query param
    public function removeQueryParam(string $key): void
    {
        unset($this->query[$key]);
    }

    // Author : Laxman
    // For parsing url and getting params in array
    private function parseQuery(string $query): array
    {
        $queryParts = explode('&', $query);
        $parsedQuery = [];

        foreach ($queryParts as $part) {
            $param = explode('=', $part, 2);
            $key = urldecode($param[0]);
            $value = isset($param[1]) ? urldecode($param[1]) : '';
            $parsedQuery[$key] = $value;
        }

        return $parsedQuery;
    }

    private function buildQuery(array $query): string
    {
        $queryParts = [];

        foreach ($query as $key => $value) {
            $key = urlencode($key);
            $value = urlencode($value);
            $queryParts[] = "{$key}={$value}";
        }

        return implode('&', $queryParts);
    }



    // Author : Laxman
    // by default, the compare method will ignore query parameters 
    public function compareUrl(Url $otherUrl, bool $ignoreQueryParams = true): bool
    {
        if ($ignoreQueryParams) {
            return $this->getBaseUrl() === $otherUrl->getBaseUrl();
        }else{
            return $this->getUrl() === $otherUrl->getUrl();
        }

    }

    
    private function getBaseUrl(): string
    {
        return "{$this->scheme}://{$this->host}{$this->path}";
    }
}
